TARGET = AKS-TF-JENKINS
DIRECTORY = terraform

clean:
	rm -rf ${DIRECTORY}/.terraform \
	rm -rf ${DIRECTORY}/.terraform.lock.hcl \
	rm -rf ${DIRECTORY}/terraform.tfstate \
	rm -rf ${DIRECTORY}/terraform.tfstate.backup \
	rm -rf ${DIRECTORY}/kubeconfig

secret:
	source ./secret.sh