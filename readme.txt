Terraform step by step
Create a new folder with the following files:
    - main.tf
    - outputs.tf

To initialize Terraform, use:
    terraform init

To perform a dry-run and inspect what Terraform will create, use:
    terraform plan

After issuing the apply command, you will be prompted to confirm, and same as before, just type "yes".

Provisioning a cluster on AKS takes, on average, about fifteen minutes.

When it's complete, if you inspect the current folder, you should notice a few new files:

$ tree .
.
├── k8s
│   ├── jenkins-ingress.yaml
│   ├── jenkins-loadbalancer.yaml
│   ├── jenkins-namespace.yaml
│   ├── jenkins-service.yaml
│   └── jenkins.yaml
├── readme.txt
├── secret.sh
└── terraform
    ├── kubeconfig
    ├── main.tf
    ├── outputs.tf
    ├── terraform.tfstate
    └── terraform.tfstate.backup


Terraform uses the terraform.tfstate to keep track of what resources were created.

The kubeconfig is the kube configuration file for the newly created cluster.

Inspect the cluster pods using the generated kubeconfig file:
    kubectl get node --kubeconfig <path-to-file-kubeconfig>
        output:
    NAME                              STATUS   ROLES   AGE     VERSION
    aks-default-75184889-vmss000000   Ready    agent   21m     v1.18.14
    aks-default-75184889-vmss000001   Ready    agent   21m     v1.18.14

If you prefer to not prefix the --kubeconfig environment variable to every command, you can export the KUBECONFIG variable as:
    export KUBECONFIG="${PWD}/kubeconfig"

    The export is valid only for the current terminal session.

Testing the cluster by deploying Jenkins-server. All files you can find in k8s folder.
You need apply yaml-files:
    1. First, need creane namespace, use:
        kubectl apply -f jenkins-namespace.yaml --kubeconfig <path-to-file-kubeconfig>
    2. Second, apply jenkins.yaml, use:
        kubectl apply -f jenkins.yaml --kubeconfig <path-to-file-kubeconfig>

        First, retrieve the name of the Pod:
        kubectl get pods -n jenkins --kubeconfig <path-to-file-kubeconfig>

        To see if your application runs correctly, you can connect to it with kubectl port-forward:
        kubectl port-forward <name_pods> 8080:8080 -n jenkins --kubeconfig <path-to-file-kubeconfig>
        If you visit http://localhost:8080 on your computer, you should be greeted by the application's web page.
    3. In Kubernetes, you can use a Service of type: LoadBalancer to start up a load balancer to expose your Pods:
        kubectl apply -f jenkins-loadbalancer --kubeconfig <path-to-file-kubeconfig>

        As soon as you submit the command, AKS provisions an L4 Load Balancer and connects it to your pod.
        Eventually, you should be able to describe the Service and retrieve the load balancer's IP address.
        Running:
            kubectl get svc -n jenkins --kubeconfig <path-to-file-kubeconfig>
        
            output:
            
            NAME               TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)         AGE
            jenkins            LoadBalancer   10.0.221.11   20.82.170.76   8080:31326/TCP  44s

        If you visit the external IP address in your browser, you should see the application.
    
    4. The load balancer that you created earlier serves one service at a time.
        Also, it has no option to provide intelligent routing based on paths.
        So if you have multiple services that need to be exposed, you will need to create the same number of load balancers.
        Imagine having ten applications that have to be exposed.
        If you use a Service of type: LoadBalancer for each of them, you might end up with ten different L4 Load Balancers.
        That wouldn't be a problem if those load balancers weren't so expensive.

    5. You should delete the previous Load Balancer Service and instead deploy the jenkins-service.yaml, so you don't end up with duplicate load balancers.
        Use this command:
            kubectl delete -f jenkins-loadbalancer.yaml --kubeconfig <path-to-file-kubeconfig>

            kubectl apply -f jenkins-service.yaml --kubeconfig <path-to-file-kubeconfig>

            kubectl apply -f jenkins-ingress.yaml --kubeconfig <path-to-file-kubeconfig>

        Check this:
            kubectl describe ingress jenkins -n jenkins --kubeconfig <path-to-file-kubeconfig>
    
            Name:             jenkins
            Namespace:        default
            Address:          20.54.107.12
            Default backend:  jenkins:8080 (10.244.1.2:8080)
            Rules:
            Host        Path  Backends
            ----        ----  --------
            *
                        /   jenkins:8080 (10.244.1.2:8080)
            Annotations:  kubernetes.io/ingress.class: addon-http-application-routing
            Events:
            Type    Reason  Age   From                      Message
            ----    ------  ----  ----                      -------
            Normal  CREATE  98s   nginx-ingress-controller  Ingress default/jenkins
            Normal  UPDATE  50s   nginx-ingress-controller  Ingress default/jenkins
    
    6. Excellent, you can use the IP from the Address field to visit your application in the browser.
       The Ingress add-on is meant as a quick way to install an Ingress and route traffic in the cluster.